package org.coderfun.common.dict.service;

import org.coderfun.common.dict.entity.CodeItem;

import klg.common.dataaccess.BaseService;

public interface CodeItemService extends BaseService<CodeItem, Long>{

}
